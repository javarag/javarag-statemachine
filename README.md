# State machine example using JavaRAG #

Static analysis for a state machine language using the embedded library JavaRAG. The analysis include reachability between states.

## How to run it ##

The static analysis can be run as follows: 

    $ git clone git@bitbucket.org:javarag/javarag-statemachine.git
    $ cd javarag-statemachine
    $ ant jar
    $ java -jar statemachine.jar examples/tiny.sm

The last command will pretty print a given state machine and also print the reachability between states. The `example` directory contains example state machines.