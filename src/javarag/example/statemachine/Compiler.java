package javarag.example.statemachine;

import java.io.FileReader;
import java.io.IOException;

import javarag.AttributeEvaluator;
import javarag.AttributeRegister;
import javarag.example.statemachine.ast.StateMachine;
import javarag.example.statemachine.attributes.ComputedNodes;
import javarag.example.statemachine.attributes.Graph;
import javarag.example.statemachine.attributes.NameAnalysis;
import javarag.example.statemachine.attributes.Reachability;
import javarag.example.statemachine.attributes.Print;
import javarag.example.statemachine.attributes.PrintReachable;
import javarag.example.statemachine.attributes.StateMachineTreeTraverser;
import javarag.example.statemachine.parser.ParseException;
import javarag.example.statemachine.parser.Parser;
import javarag.impl.reg.BasicAttributeRegister;

public class Compiler {
	public static void main(String[] args) throws ParseException, IOException, InterruptedException {
		if (args.length != 1) {
			System.err.println("Please specify a filename!");
			System.exit(1);
		}
		
		String filename = args[0];
		
		AttributeRegister registry = new BasicAttributeRegister();
		registry.register(Print.class, NameAnalysis.class, Graph.class, Reachability.class, PrintReachable.class, ComputedNodes.class);
		
		Parser parser = new Parser(new FileReader(filename));
		StateMachine stateMachine = parser.parse();
		
		AttributeEvaluator evaluator = registry.getEvaluator(stateMachine, new StateMachineTreeTraverser());
		
		evaluator.evaluate("print", stateMachine);
		evaluator.evaluate("printReachable", stateMachine);
	}
}
