package javarag.example.statemachine.attributes;

import java.util.Set;

import javarag.Module;
import javarag.NonTerminal;
import javarag.Synthesized;
import javarag.coll.Collector;
import javarag.example.statemachine.ast.Declaration;
import javarag.example.statemachine.ast.State;
import javarag.example.statemachine.ast.StateMachine;
import javarag.example.statemachine.ast.Transition;

public class ComputedNodes extends Module<ComputedNodes.Attributes> {

	public interface Attributes {
		@NonTerminal
		@Synthesized
		public Transition extraTransition(StateMachine sm);

		@NonTerminal
		@Synthesized
		public State extraState(StateMachine sm);

		public Set<Declaration> declarations(StateMachine sm);
	}

	public void declarations(StateMachine sm, Collector<Declaration> coll) {
		coll.add(sm, e().extraTransition(sm));
		coll.add(sm, e().extraState(sm));
	}

	public void successors(StateMachine sm, Collector<State> coll) {
		coll.collectFrom(e().extraTransition(sm));
	}

	public Transition extraTransition(StateMachine sm) {
		return new Transition("extra", "extra", "extra");
	}

	public State extraState(StateMachine sm) {
		return new State("extra");
	}

}
