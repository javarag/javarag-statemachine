package javarag.example.statemachine.attributes;

import java.util.HashSet;
import java.util.Set;

import javarag.Bottom;
import javarag.Circular;
import javarag.Module;
import javarag.Synthesized;
import javarag.example.statemachine.ast.State;

public class Reachability extends Module<Reachability.Interface> {
	public interface Interface extends Graph.Interface {
		@Synthesized
		@Circular
		Set<State> reachable(State source);
	}

	@Bottom("reachable")
	public Set<State> reachableStart(State source) {
		return new HashSet<>();
	}

	public Set<State> reachable(State source) {
		Set<State> result = new HashSet<State>();
		Set<State> successors = e().successors(source);
		for (State s : successors) {
			result.add(s);
			Set<State> reachable = e().reachable(s);
			result.addAll(reachable);
		}
		return result;
	}

}
