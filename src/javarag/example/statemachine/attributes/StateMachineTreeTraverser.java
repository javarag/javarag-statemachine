package javarag.example.statemachine.attributes;

import java.util.Collections;

import javarag.TreeTraverser;
import javarag.example.statemachine.ast.Declaration;
import javarag.example.statemachine.ast.StateMachine;

public class StateMachineTreeTraverser implements TreeTraverser<Object> {

	@Override
	public Iterable<? extends Object> getChildren(Object root) {
		if (root instanceof StateMachine) {
			Iterable<Declaration> decls = ((StateMachine) root).getDeclarations();
			return decls;
		}
		return Collections.emptyList();
	}

}
