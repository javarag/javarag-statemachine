package javarag.example.statemachine.attributes;

import java.util.Set;

import javarag.Module;
import javarag.Procedural;
import javarag.example.statemachine.ast.Declaration;
import javarag.example.statemachine.ast.State;
import javarag.example.statemachine.ast.StateMachine;
import javarag.example.statemachine.ast.Transition;

public class Print extends Module<Print.Interface> {
	public interface Interface {
		@Procedural
		void print(Declaration decl);

		@Procedural
		void print(StateMachine stateMachine);

		Set<Declaration> declarations(StateMachine stateMachine);
	}

	public void print(StateMachine stateMachine) {
		for (Declaration decl : e().declarations(stateMachine)) {
			e().print(decl);
		}
	}

	public void print(State state) {
		System.out.println("state " + state.getLabel() + ";");
	}

	public void print(Transition trans) {
		System.out.println("trans " + trans.getLabel() + " : " +
				trans.getSourceLabel() + " -> " +
				trans.getTargetLabel() + ";");
	}

}
