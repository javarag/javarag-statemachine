package javarag.example.statemachine.attributes;

import java.util.Set;

import javarag.Module;
import javarag.Procedural;
import javarag.example.statemachine.ast.Declaration;
import javarag.example.statemachine.ast.State;
import javarag.example.statemachine.ast.StateMachine;

public class PrintReachable extends Module<PrintReachable.Interface> {
	public interface Interface extends Reachability.Interface {
		@Procedural
		void printReachable(StateMachine stateMachine);

		@Procedural
		void printReachable(Declaration decl);

		Set<Declaration> declarations(StateMachine node);
	}

	public void printReachable(StateMachine node) {
		for (Declaration d : e().declarations(node)) {
			e().printReachable(d);
		}
	}

	public void printReachable(Declaration node) {
		return;
	}

	public void printReachable(State node) {
		System.out.print(node.getLabel() + " can reach { ");
		Set<State> reachable = e().reachable(node);
		boolean first = true;
		for (State r : reachable) {
			if (first) {
				first = false;
			} else {
				System.out.print(", ");
			}
			System.out.print(r.getLabel());
		}
		System.out.println(" }");
	}

}
