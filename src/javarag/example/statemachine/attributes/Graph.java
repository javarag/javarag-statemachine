package javarag.example.statemachine.attributes;

import java.util.Set;

import javarag.Collected;
import javarag.Module;
import javarag.coll.Builder;
import javarag.coll.Builders;
import javarag.coll.Collector;
import javarag.example.statemachine.ast.State;
import javarag.example.statemachine.ast.Transition;

public class Graph extends Module<Graph.Interface> {

	public interface Interface {
		@Collected
		Set<State> successors(State s);

		State source(Transition t);

		State target(Transition t);
	}

	public Builder<Set<State>, State> successors(State s) {
		return Builders.setBuilder();
	}

	public void successors(Transition t, Collector<State> collector) {
		State source = e().source(t);
		State target = e().target(t);
		collector.add(source, target);
	}
}
