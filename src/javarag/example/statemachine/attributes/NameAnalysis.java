package javarag.example.statemachine.attributes;

import java.util.Set;

import javarag.Cached;
import javarag.Collected;
import javarag.Inherited;
import javarag.Module;
import javarag.Synthesized;
import javarag.coll.Builder;
import javarag.coll.Builders;
import javarag.coll.Collector;
import javarag.example.statemachine.ast.Declaration;
import javarag.example.statemachine.ast.State;
import javarag.example.statemachine.ast.StateMachine;
import javarag.example.statemachine.ast.Transition;

public class NameAnalysis extends Module<NameAnalysis.Interface> {
	public interface Interface {
		@Synthesized
		@Cached
		State source(Transition t);

		@Synthesized
		@Cached
		State target(Transition t);

		@Inherited
		State lookup(Transition node, String label);

		@Synthesized
		State localLookup(Declaration decl, String label);
		
		@Collected
		Set<Declaration> declarations(StateMachine node);
	}
	
	public Builder<Set<Declaration>, Declaration> declarations(StateMachine sm) {
		return Builders.setBuilder();
	}
	
	public void declarations(StateMachine sm, Collector<Declaration> coll) {
		for (Declaration d : sm.getDeclarations()) {
			coll.add(sm, d);
		}
	}

	public State lookup(StateMachine node, String label) {
		for (Declaration decl : e().declarations(node)) {
			State s = e().localLookup(decl, label);
			if (s != null)
				return s;
		}
		return null;
	}

	public State localLookup(State node, String label) {
		if (node.getLabel().equals(label)) {
			return node;
		} else {
			return null;
		}
	}

	public State localLookup(Declaration node, String label) {
		return null;
	}

	public State source(Transition node) {
		return e().lookup(node, node.getSourceLabel());
	}

	public State target(Transition node) {
		return e().lookup(node, node.getTargetLabel());
	}

}
