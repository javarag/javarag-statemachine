package javarag.example.statemachine.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StateMachine {
	private final List<Declaration> declarations;

	public StateMachine(List<Declaration> declarations) {
		this.declarations = Collections.unmodifiableList(new ArrayList<>(declarations));
	}

	public List<Declaration> getDeclarations() {
		return declarations;
	}
}
