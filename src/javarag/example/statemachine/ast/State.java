package javarag.example.statemachine.ast;

public class State extends Declaration {
	private final String label;

	public State(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
	@Override
	public String toString() {
		return "State(\"" + label + "\")";
	}
}
