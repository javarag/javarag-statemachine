package javarag.example.statemachine.ast;

public class Transition extends Declaration {
	private final String label, sourceLabel, targetLabel;

	public Transition(String label, String sourceLabel, String targetLabel) {
		this.label = label;
		this.sourceLabel = sourceLabel;
		this.targetLabel = targetLabel;
	}

	public String getLabel() {
		return label;
	}

	public String getSourceLabel() {
		return sourceLabel;
	}

	public String getTargetLabel() {
		return targetLabel;
	}

}
